from flask import Flask, render_template, request, redirect, url_for
from init import convert

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        compound = request.form['compound']
        answer = convert(compound)
        return render_template('compound.html', answer=answer)
    return render_template('index.html')

@app.route('/blog.html')
def blog():
    return render_template('blog.html')

@app.route('/assign.html')
def assign():
    return render_template('assign.html')

@app.route('/due.html')
def due():
    return render_template('due.html')

@app.route('/peta.html')
def peta():
    return render_template('peta.html')

@app.route('/exam.html')
def exam():
    return render_template('exam.html')

@app.route('/genchem.html', methods=['GET', 'POST'])
def genchem():
    return render_template('genchem.html')

