import re
from peritable import peritab, befide, cantion

def convert(compound):
    # checks if 2nd and 3rd letter is upper case, if yes then it is a formula
    if any(letter.isupper() for letter in compound[1:4]):
        # check how many uppercase are in compound from letter 2 to 6, if only 2 uppercase/elements then it is binary
        uppercount = sum(1 for letter in compound[1:6] if letter.isupper())
        if uppercount <= 1:
            # turn the 2 elements into a list
            elementS = re.findall("[A-Z][a-z]*", compound)

            # make empty list of element name
            elementN = []
            for item in elementS:
                for pertivalues in peritab.values():
                    if item == pertivalues["symbol"]:
                        elementN.append(pertivalues["name"])

            # check if first element symbol is a class-metal and checks second element symbol if it is nonmetal, if so then it is ionic
            if peritab[elementS[0]]["class"] == "metal" and peritab[elementS[1]]["class"] == "nonmetal":
                for end in befide:
                    if elementN[-1].endswith(end):
                        if elementN[-1] in cantion:
                            return "cannot form ionic compound with these elements"
                        else:
                            ans = elementN[-1].replace(end, "ide")
                            return peritab[elementS[0]]['name'] + " " + ans
            else:
                return "this is not an ionic compound"

        else:
            return "this is ternary"

    else:
        compoundS = compound.split()
        for item in compoundS:
            if re.search(r"hydr|ate|\(\)", item):
                return "this is ternary"
        return compoundS
